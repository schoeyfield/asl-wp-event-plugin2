<?php
/**
 *  Install Add-ons
 *  
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *  
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme/plugin as outlined in the terms and conditions.
 *  For more information, please read:
 *  - http://www.advancedcustomfields.com/terms-conditions/
 *  - http://www.advancedcustomfields.com/resources/getting-started/including-lite-mode-in-a-plugin-theme/
 */ 

// Add-ons 
// include_once('add-ons/acf-repeater/acf-repeater.php');
// include_once('add-ons/acf-gallery/acf-gallery.php');
// include_once('add-ons/acf-flexible-content/acf-flexible-content.php');
// include_once( 'add-ons/acf-options-page/acf-options-page.php' );


/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

add_post_meta( 12, 'flavor', 'vanilla' );

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brief-instructions-for-reviewing',
		'title' => 'Brief Instructions for Reviewing',
		'fields' => array (
			array (
				'key' => 'field_524c9f6e0ebb3',
				'label' => 'Brief Instructions',
				'name' => '',
				'type' => 'message',
				'message' => '<p><b>Thank you</b> so much for taking the time to review something for us. Reviews are dynamically syndicated from The Spotlight across the library\'s websites. There can be multiple reviews for the same book [or any format], so don\'t worry if a review already exists. Especially, we challenge you that if you see a review you <i>disagree</i> with, write your own counterpoint : ). </p>
	
	<p><b>How long should the review be?</b> <br> As long as you want, but preferably at least one full paragraph. Remember that reviews are largely supplementary. Patrons care what friends <i>and readers with certain authority (you!)</i> recommend. They will probably make their decision at a glance. Don\'t let that hold you back, though!</p>
	
	<p>
	<b>The Process</b>
	<ol>
	<li>Write a title for <i>the review</i>, not the work itself. Pretend this is a headline for a netmag. In hindsight, you might say of the first Harry Potter book: "Don\'t let The Sorcerer\'s Stone keep you from the rest of the series." Something like that.</li>
	
	<li>Work through the fields below. Enter the title and author[s], link it to NovaCat, and rate it.</li>
	
	<li> Write the review in the main content area.</li>
	<li> Work through just the "General" tab of the SEO section below. </li>
	<li>Upload a book cover under "Featured Image" on the right.	</li>
	</ol>
	</p>',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'spotlight_reviews',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_rock-that-review-librarian',
		'title' => 'Rock that review, librarian!',
		'fields' => array (
			array (
				'key' => 'field_524c798fff602',
				'label' => 'Material Type',
				'name' => 'material_type',
				'type' => 'select',
				'instructions' => 'We will add more material types and formats as the kinks are ironed out. For now, chalk-up formats like "audiobook" or "ebook" to "book". ',
				'required' => 1,
				'choices' => array (
					'Book' => 'Book',
					'Video Game' => 'Video Game',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_524c86e982e03',
				'label' => 'Title',
				'name' => 'title',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Hyperion',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_524c871d82e04',
				'label' => 'Author',
				'name' => 'author',
				'type' => 'text',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_524c798fff602',
							'operator' => '==',
							'value' => 'Book',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'Dan Simmons',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_524c8846d3ed1',
				'label' => 'Console',
				'name' => 'review_game_console',
				'type' => 'checkbox',
				'instructions' => 'Not only referring to the console-version <i>you</i> played, but consoles for which the libraries have copies. We don\'t want to give the false impression that "Game X" is available in the library for both the Xbox 360 and PS3 if it isn\'t.',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_524c798fff602',
							'operator' => '==',
							'value' => 'Video Game',
						),
					),
					'allorany' => 'all',
				),
				'choices' => array (
					'xbox' => 'Xbox',
					360 => 'Xbox 360',
					'x1' => 'Xbox One',
					'ps2' => 'PlayStation 2',
					'ps3' => 'PlayStation 3',
					'wii: Wii' => 'wii: Wii',
					'wiiu: Wii U' => 'wiiu: Wii U',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_524c766baa536',
				'label' => 'Link to NovaCat',
				'name' => 'link_to_novacat',
				'type' => 'text',
				'instructions' => 'Let patrons jump on that bandwagon!',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'http://novacat.nova.edu:80/record=b1353323~S13',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_524c7ab5999fe',
				'label' => 'So, what did you think?',
				'name' => 'review_scale',
				'type' => 'select',
				'instructions' => 'Using an 11-point review scale where <b>10 is a Masterpiece</b> and <b>0 is a disaster</b>, tell everyone what you thought!',
				'required' => 1,
				'choices' => array (
					10 => '10 - Masterpiece',
					9 => '9 - Amazing',
					8 => '8 - Great',
					7 => '7 - Good',
					6 => '6 - Okay',
					5 => '5 - Mediocre',
					4 => '4 - Bad',
					3 => '3 - Awful',
					2 => '2 - Painful',
					1 => '1 - Unbearable',
					0 => '0 - Disaster',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_524c9b51c91cc',
				'label' => 'Who\'s the Reviewer?',
				'name' => 'reviewed_by_type',
				'type' => 'select',
				'instructions' => 'You can review on your own behalf or on behalf of a patron. <b>Note</b>: right now, until the disclaimers and necessary policies are hammered out, you can only review on your own behalf. Why ask? We want to be able to disassociate staff from patron reviews. ',
				'choices' => array (
					'staff' => 'Me!',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_524c9e9db3559',
				'label' => 'What\'s your first name?',
				'name' => 'reviewed_by',
				'type' => 'text',
				'instructions' => 'Or, if you\'re wary, you can make one up. Names give content the human touch. This would be presented, for example, as: "[ review here ] - <i> by Michael </i>." ',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'spotlight_reviews',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	
	register_field_group(array (
		'id' => 'acf_important-information-about-the-resource',
		'title' => 'Important Information about the Resource',
		'fields' => array (
			array (
				'key' => 'field_5216801549741',
				'label' => 'Authenticating URI',
				'name' => 'authenticated_url',
				'type' => 'text',
				'instructions' => 'Databases won\'t work for patrons unless they are proxied through our system. You can copy the authenticating link by visiting <a href="http://sherman.library.nova.edu/e-library" target="new">our e-library indices</a>. You know you have the right link if it ends with "<b>aid=</b>" and a number. Sorry, we know it\'s kind of a pain.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'example: http://0-auth.novasoutheastern.org.novacat.nova.edu/go/redirect.php?aid=721',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_523089e1be8b4',
				'label' => 'Resource AID',
				'name' => 'aid',
				'type' => 'number',
				'instructions' => 'You know that link you just pasted with <b>aid=</b> [some number]? Type that number here. ',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'example: 769',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => 0,
			),
			array (
				'key' => 'field_54b5359cc1c2d',
				'label' => 'App Store',
				'name' => 'has_app',
				'type' => 'checkbox',
				'instructions' => 'If this database has an app, check the app stores where it can be downloaded.',
				'choices' => array (
					'android' => 'Google Play',
					'ios' => 'Apple',
					'kindle' => 'Kindle',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_54b53635c1c2e',
				'label' => 'Google Play Link',
				'name' => 'google_play_link',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54b5359cc1c2d',
							'operator' => '==',
							'value' => 'android',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54b536d6665e8',
				'label' => 'Apple Link',
				'name' => 'apple_link',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54b5359cc1c2d',
							'operator' => '==',
							'value' => 'ios',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54b536ef665e9',
				'label' => 'Kindle Link',
				'name' => 'kindle_link',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54b5359cc1c2d',
							'operator' => '==',
							'value' => 'kindle',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			/*array (
				'key' => 'field_52334cb4f5185',
				'label' => 'Advertisement Image',
				'name' => 'ad_image',
				'type' => 'image',
				'instructions' => 'Select or upload an image for use as an ad. The dimensions of the image must be 16:9. You can use <a href="http://size43.com/jqueryVideoTool.html" target="new"> this tool	</a> to easily calculate pixels.<b>Once set, you must also set the database logo under "Featured Image"</b>.',
				'required' => 0,
				'save_format' => 'url',
				'preview_size' => 'media-small',
				'library' => 'all',
			),*/
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'spotlight_databases',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'discussion',
				1 => 'comments',
				2 => 'revisions',
				3 => 'slug',
				4 => 'author',
				5 => 'format',
				6 => 'send-trackbacks',
			),
		),
		'menu_order' => 1,
	));
	/*register_field_group(array (
		'id' => 'acf_create-an-awesome-book-list',
		'title' => 'Create an Awesome Book List',
		'fields' => array (
			/*array (
				'key' => 'field_53358ed452b89',
				'label' => 'Upload Text File',
				'name' => 'list_file',
				'type' => 'file',
				'instructions' => 'Create a list in Millenium / Sierra with the necessary fields and export it as a .txt file, then upload it here.',
				'required' => 0,
				'save_format' => 'object',
				'library' => 'uploadedTo',
			),*/
			/*array (
				'key' => 'field_533595120d071',
				'label' => 'Feature an Item from this List',
				'name' => '',
				'type' => 'message',
				'message' => 'Give this list a "face" by picking one of the items on it and featuring it below. You\'ll need to enter the <strong>OCLC No.</strong> and the <strong>ISBN</strong>, which is what we\'ll use to fetch a book cover. Then just add the title, the author, and a brief summary - maybe your own, from Amazon or Goodreads, or from the record itself.',
			),
			array (
				'key' => 'field_533596410d075',
				'label' => 'Title',
				'name' => 'list_feature_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5335964e0d076',
				'label' => 'Author',
				'name' => 'list_feature_author',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5335965b0d077',
				'label' => 'Summary',
				'name' => 'list_feature_summary',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5335960e0d073',
				'label' => 'Bib. Record Number',
				'name' => 'list_feature_brecord',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_533595f80d072',
				'label' => 'OCLC Number',
				'name' => 'list_feature_oclc',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_533596370d074',
				'label' => 'ISBN',
				'name' => 'list_feature_isbn',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'list',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));*/
	register_field_group(array (
		'id' => 'acf_supplementary-information-about-the-resource',
		'title' => 'Supplementary Information about the Resource',
		'fields' => array (
			array (
				'key' => 'field_52334c236f4d3',
				'label' => 'Instructions',
				'name' => 'instructions',
				'type' => 'wysiwyg',
				'instructions' => 'Some resources require users to create an account even after authenticating with their library card. If there is some element to this database that is confusing or poorly explained by vendor, type-up brief instructions. <b>Please note:</b> instructions more than a paragraph long should be included as handouts. This spot isn\'t for those.',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_52334b8a780ee',
				'label' => 'LibraryLearn Screencast',
				'name' => 'screencast',
				'type' => 'text',
				'instructions' => 'Optionally link to a relevant <b>LibraryLearn</b> tutorial.',
				'default_value' => '',
				'placeholder' => 'http://sherman.library.nova.edu/sites/tutorials/watch/a-helpful-video/',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52334bdd780ef',
				'label' => 'Written Tutorial, Handout, or Guide',
				'name' => 'handout',
				'type' => 'text',
				'instructions' => 'Optionally link to a relevant <b>LibGuide</b>, handout, or other detailed instructions.',
				'default_value' => '',
				'placeholder' => 'http://nova.campusguides.com/a-helpful-tutorial',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'spotlight_databases',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));

	// Event Information
	register_field_group(array (
		'id' => 'acf_event-information',
		'title' => 'Event Information',
		'fields' => array (

			array (
				'key' => 'field_5578597ea9s38',
				'label' => 'Basic Info',
				'name' => '',
				'type' => 'tab',
			),

			array (
				'key' => 'field_5592b2a23b743',
				'label' => 'A Short Description',
				'name' => 'asl_event_excerpt',
				'type' => 'textarea',
				'instructions' => '<p>A one-to-three sentence lede to your event. It is <strong>your event at a glance</strong>, and this is what we show when events are embedded and on social media. <strong>Use the main content area below</strong> to embed videos, link, embed books, post pictures, or otherwise elaborate.</p>',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '280',
				'rows' => '4',
				'formatting' => 'br',
			),

			/*array (
				'key' => 'field_558b01737cbf9aslwp',
				'label' => 'Who is this for?',
				'name' => 'asl_library_audience',
				'type' => 'taxonomy',
				'instructions' => '<p>You can choose as many as these <strong>broad library audiences</strong> as makes sense for the kind of event you\'re making. This is for pages that pull in lists dynamically by patron-type.</p>',
				'required' => 1,
				'taxonomy' => 'library-audience',
				'field_type' => 'checkbox',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),*/

			array (
				'key' => 'field_5293e160fecc1',
				'label' => 'Location',
				'name' => 'location',
				'type' => 'taxonomy',
				'taxonomy' => 'location',
				'field_type' => 'select',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),

			array (
				'key' => 'field_544673c4a115d',
				'label' => 'Add contact instructions?',
				'name' => 'contact_person_options',
				'type' => 'radio',
				'choices' => array (
					'no' => 'No',
					'yes' => 'Yes',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'no',
				'layout' => 'horizontal',
			),
			/*array (
				'key' => 'field_5446747aa115e',
				'label' => 'Library staff member',
				'name' => 'library_staff_member',
				'type' => 'user',
				'instructions' => 'You can select a specific library staff member, as long as she or he has logged into the Sherman WordPress Network at least once. Contact information is set in that user\'s profile.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_544673c4a115d',
							'operator' => '==',
							'value' => 'yes',
						),
					),
					'allorany' => 'all',
				),
				'role' => array (
					0 => 'all',
				),
				'field_type' => 'select',
				'allow_null' => 1,
			),*/

			array (
				'key' => 'field_544675f4c170c',
				'label' => 'Contact Instructions',
				'name' => 'other_contact_instructions',
				'type' => 'wysiwyg',
				'instructions' => 'Optionally write contact instructions. For instance, you can choose to designate a specific library staff member or leave general contact information for the Public or Reference Desk.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_544673c4a115d',
							'operator' => '==',
							'value' => 'yes',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),

			/*array (
				'key' => 'field_5564d3bd7534b',
				'label' => '[Deprecated] Is there a call to action?',
				'name' => 'has_c2a',
				'type' => 'radio',
				'instructions' => 'Please note: these options will be removed on <b>October 5, 2015</b>.',
				'choices' => array (
					'no' => 'No',
					'yes' => 'Yes',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'no',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5564d5602a20a',
				'label' => 'Call to action',
				'name' => 'c2a_text',
				'type' => 'text',
				'instructions' => '<b>What should the button say</b>? It needs to be brief (barely a phrase) and, well, a <em>call to action</em>. E.g., "<b>Register</b>," "<b>Sign Up</b>", "<b>Download</b>", "<b>Tell Your Friends</b>", "<b>Leave Feedback</b>".',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5564d3bd7534b',
							'operator' => '==',
							'value' => 'yes',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'Register',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 20,
			),
			array (
				'key' => 'field_5564d7312a20b',
				'label' => 'Action link',
				'name' => 'c2a_link',
				'type' => 'text',
				'instructions' => '<b>Where do you want the patron to go when they click the button?</b> This might be a link to the registration form, for instance. ',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5564d3bd7534b',
							'operator' => '==',
							'value' => 'yes',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'http://www.libsurveys.com/loader.php?id=f8527f5beeaadd6d3c86de44f7d08222',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),*/

			array (
				'key' => 'field_557aslwp597ea9s38',
				'label' => 'Date and Time',
				'name' => '',
				'type' => 'tab',
			),

			array (
				'key' => 'field_527437b7a09f8',
				'label' => 'Date and Time',
				'name' => 'scheduling_options',
				'type' => 'checkbox',
				'instructions' => 'There are slightly different scheduling options depending on whether your event is all-day or multi-day. If your multi-day event is also pretty much all day, don\'t forget to check both! Here are <a href="http://sherman.library.nova.edu/sites/labs/using-wordpress-network/#events" target="new">more detailed instructions for creating an event</a>.',
				'choices' => array (
					'allday' => 'An all-day event',
					'multiday' => 'A multi-day event',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_52743546181f4',
				'label' => 'Day of Event',
				'name' => 'event_start',
				'type' => 'date_picker',
				'required' => 1,
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52743665181f5',
				'label' => 'From',
				'name' => 'event_start_time',
				'type' => 'date_time_picker',
				'default_value' => '12:00 am',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_527437b7a09f8',
							'operator' => '!=',
							'value' => 'allday',
						),
					),
					'allorany' => 'all',
				),
				'show_date' => 'false',
				'date_format' => 'm/d/y',
				'time_format' => 'h:mm tt',
				'show_week_number' => 'false',
				'picker' => 'slider',
				'save_as_timestamp' => 'false',
				'get_as_timestamp' => 'false',
			),
			array (
				'key' => 'field_52743b73aadaf',
				'label' => 'Until',
				'name' => 'event_end_time',
				'type' => 'date_time_picker',
				'default_value' => '12:01 am',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_527437b7a09f8',
							'operator' => '!=',
							'value' => 'allday',
						),
					),
					'allorany' => 'all',
				),
				'show_date' => 'false',
				'date_format' => 'm/d/y',
				'time_format' => 'h:mm tt',
				'show_week_number' => 'false',
				'picker' => 'slider',
				'save_as_timestamp' => 'false',
				'get_as_timestamp' => 'false',
			),
			array (
				'key' => 'field_52743ba6aadb0',
				'label' => 'Last Day of Event',
				'name' => 'event_end',
				'type' => 'date_picker',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_527437b7a09f8',
							'operator' => '==',
							'value' => 'multiday',
						),
					),
					'allorany' => 'all',
				),
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52743bfa3dd5d',
				'label' => 'Cost',
				'name' => 'event_cost',
				'type' => 'number',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_527437b7a09f8',
							'operator' => '==',
							'value' => 'costs',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 0,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => '',
				'step' => '',
			),
			
			array (
				'key' => 'field_557597ea9s38aslwp',
				'label' => 'Series and Format',
				'name' => '',
				'type' => 'tab',
			),

			array (
				'key' => 'field_558b01737cbf9aslevent',
				'label' => 'Format / Event Type',
				'name' => 'asl_event_type',
				'type' => 'taxonomy',
				'instructions' => '<p>An <strong>event type</strong> is more about format than subject, but there are some outliers. Choose as many as make sense.</p>',
				'required' => 0,
				'taxonomy' => 'event_type',
				'field_type' => 'checkbox',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),

			array (
				'key' => 'field_558b01737cbf9aslseries',
				'label' => 'Series',
				'name' => 'asl_event_series',
				'type' => 'taxonomy',
				'instructions' => '<p>Choose as many <strong>series</strong> to which this event belongs. Some have a parent/child relationhip, e.g. "Library Boot Camp" is automatically part of "Academic Workshops". Need a series created? <a href="http://sherman.library.nova.edu/sites/labs/#new-ticket" target="new">No problem</a>!</p>',
				'required' => 0,
				'taxonomy' => 'series',
				'field_type' => 'checkbox',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),

		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'spotlight_events',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'excerpt',
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_library-staff-member-information',
		'title' => 'Library Staff Member Information',
		'fields' => array (
			array (
				'key' => 'field_544679a91078b',
				'label' => 'Contact Information',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_544678d3ee98e',
				'label' => 'Office or Desk Phone',
				'name' => 'asl_phone_number',
				'type' => 'text',
				'instructions' => 'ex: (954) 262-4536',
				'default_value' => '',
				'placeholder' => '(954) 262-####',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 14,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_user',
					'operator' => '==',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));
	
}

?>