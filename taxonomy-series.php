<?php get_header(); ?>
	
	<header class="hero has-background background-base">
		<h1 class="align-center title"><?php single_cat_title(); ?></h1>
		<div class="col-md--eightcol col--centered"><?php echo term_description(); ?></div>
	</header>		


			<div id="content">
			
				<div id="inner-content" class="clearfix">
				
				    <main id="main" class="col-md--eightcol col--centered hero clearfix" role="main">

				    	<?php get_template_part( 'loop', 'event' ); ?>
			
    				</main> <!-- end #main -->
                    
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>