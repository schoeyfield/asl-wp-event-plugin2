<?php get_header(); ?>
<?php wp_redirect('http://nova.edu/library/main'); ?>
<?php if ( has_post_thumbnail() ) : ?>
	<div class="feature feature-image" id="feature" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>); ">
		<div id="inner-feature" class="clearfix">

			<div class="caption shadow">
				<article class="wrap clearfix">
						<header>
							<h1 class="page-title gamma" itemprop="headline"><?php the_title(); ?></h1>
						</header>
						<p class="epsilon">
							<strong>The Spotlight</strong> is the Alvin Sherman Library's blog - sort of. This is the
							hub where a lot of our content originates! We post events here, create book lists and
							make staff picks, post any news, spotlight hidden gems and databases, and more. 
						</p>
				</article>
			</div>
		</div><!--.inner-feature-->
	</div>
<?php endif; ?>			

<?php get_template_part( 'template-search' ); ?>

	<!-- Collection Types
	======================
	-->	<nav class="align-center panels clearfix">

			<div class="panel one-third compose">
				<a class="align-bottom button" href="<?php echo get_permalink( 1623 ); ?>" title="<?php echo get_the_title( 1623 ); ?>">
					The Blog
				</a>
			</div>

			<div class="panel one-third research">
				<a class="align-bottom button" href="<?php echo get_post_type_archive_link( 'spotlight_databases' ); ?>" title="Databases and Resources in the Alvin Sherman E-Library">
					Featured Resources
				</a>
			</div>


			<div class="panel one-third event">
				<a class="align-bottom button" href="<?php echo get_post_type_archive_link( 'spotlight_events' ); ?>" title="Upcoming Events, Classes, Exhibits, and more.">
					Upcoming Events
				</a>
			</div>

		</nav>

		<div id="content">
			<div id="inner-content" class="wrap clearfix">

				<main id="main" class="eightcol last clearfix">	

					<h2 class="hide-accessible">Latest Posts</h2>
					<?php get_template_part( 'partials/loop-post' ); ?>
	
				</main>

				<?php get_sidebar(); ?>

			</div><!--//#inner-content-->
		</div>

<?php get_footer(); ?>
