<!-- Database Card
======================
-->	<?php 
	
	// Default Parameters
	$args = array(
		'post_type' => 'spotlight_databases',
		'posts_per_page' => 1
	); 

	// If Archive
	if ( is_archive( 'spotlight_databases' ) ) {
		$args = array(
			'post_type' => 'spotlight_databases',
			'posts_per_page' => 5
		);
	}

	$i = 0;
	$featured = new WP_Query ( $args ); 

	if ( $featured->have_posts() ) : while ( $featured->have_posts() ) : $i++; $featured->the_post();
	
	/* ==================
	 * Content
	 */

	/* ==================
	 *  Layout Options
	 */ $title 					= get_post_meta( get_the_ID(), 'overlay_title', true );
		if ( !$title ) {
			$title = get_the_title();
		}

	?>
		

		<section class="resource media clearfix"><a href="<?php echo get_permalink()?>" itemprop="url">			
			<?php echo get_the_post_thumbnail( get_the_ID(), 'media-small' ); ?>		

		<article class="card" itemscope itemtype="http://schema.org/Event">
						
			<header>	

				<?php echo get_the_term_list( $post->ID, 'resource-subjects' ); ?>						

				<h3 class="epsilon no-margin title" itemprop="name">
					<?php echo $title; ?>					
				</h3>

			</header>

		</article></a>
		</section>		
		

<?php endwhile; endif; ?>