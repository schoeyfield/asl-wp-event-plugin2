<?php
/**
 * Now, we have a copy of the repository on our desktop.
 * This is where we make the changes - such as adding this
 * comment.
 *
 * Once you're satisfied, you need to
 * 1. Commit your changes
 * 2. Push back to YOUR repository
 * 3. Go into YOUR repository, and create PULL REQUEST to SHERMANLIBRARY.
 *
 * Let me show you :). 
 */

/**
 *  $VARIABLES
 */
$heroBackgroundColor = '#e2624f';

if ( get_query_var( 'for' ) ) :

  $audience = get_query_var( 'for' );

  if ( $audience === 'kids' ) :
    $heroBackgroundColor = '#dd7129';

  elseif( $audience === 'teens' ) :
    $heroBackgroundColor = '#4fb456';

  else :
    $heroBackgroundColor = '#4991cc';
  endif;

endif;

?>

<?php get_header(); ?>

<div id="content" data-ng-controller="AdController as adc" data-audience="summer<?php echo ( $audience ? '-' . $audience : '' ); ?>">

  <!-- Top of the page hero -->
  <div class="has-background" data-ng-repeat="ad in adc.ads | limitTo : 1" style="background-color: <?php echo $heroBackgroundColor; ?>;">

    <div class="clearfix wrap">
      <div class="ad ad--hero ad--transparent card">


        <div class="col-md--sixcol ad__media align-center">
          <img src="" alt="" ng-src="{{ad.media}}">
        </div>

        <div class="col-md--sixcol">
          <div class="col-md--tencol col--centered ad__copy">
            <div class="card__header">
              <h2 class="menu__item__title">{{ ad.title }}</h2>
            </div>
            <div class="card__content">
              <p>{{ ad.excerpt }}</p>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>

  <!--Adults/Teens/Kids Navigation-->
  <?php if ( !get_query_var( 'for' ) ) : ?>
  <nav class="clearfix hero wrap" role="navigation">

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=adults" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summeradults.jpg" alt="Adult programs">
          </div>
        </div>
      </a>
    </div>

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=teens" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summerteens.jpg" alt="Teen programs">
          </div>
        </div>
      </a>
    </div>

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=kids" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summerkids.jpg" alt="Kids programs">
          </div>
        </div>
      </a>
    </div>

  </nav>
  <?php endif; ?>

  <div class="has-cards hero">

    <div class="clearfix wrap">

    <!--Sidebar-->
    <div class="col-md--fourcol">

      <nav role="navigation" class="menu menu--sidebar">
        <?php if ( get_query_var( 'for' ) ) : ?>
        <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/">Back to summer</a>
        <?php endif; ?>
        <a href="#" class="link link--undecorated">One</a>
        <a href="#" class="link link--undecorated">Two</a>
        <a href="#" class="link link--undecorated">Tthree</a>
      </nav>

      <ng-repeat data-ng-repeat="ad in adc.ads" data-ng-if="$index > <?php echo ( get_query_var( 'for' ) ? '2' : '0' ); ?>">
        <a ng-href="{{ ad.link }}" class="link link--undecorated">
          <div class="card">
            <div class="card__media">
              <img alt="ad.title" ng-src="{{ad.media}}">
            </div>
            <div class="card__header">
              <h2 class="card__title">{{ ad.title }}</h2>
            </div>
            <div class="card__content">
              <p class="zeta">{{ad.excerpt}}</p>
            </div>
          </div>
        </a>
      </ng-repeat>

    </div>

    <main class="col-md--eightcol" role="main">

      <?php if ( get_query_var( 'for' ) ) : ?>
      <div class="clearfix hero--small">
        <div class="col-md--sixcol" data-ng-repeat="ad in adc.ads | limitTo : 3" data-ng-if="$index > 0">
          <a ng-href="{{ ad.link }}" class="link link--undecorated">
            <div class="card">
              <div class="card__media">
                <img alt="" ng-src="{{ad.media}}">
              </div>
              <div class="card__header">
                <h2 class="card__title">{{ ad.title }}</h2>
              </div>
              <div class="card__content">
                <p class="zeta">{{ad.excerpt}}</p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <?php endif; ?>

      <!-- Sign-up form -->
      <?php if ( !get_query_var( 'for' ) || get_query_var( 'for' ) === 'kids' ) :?>
      Kids or Default Form
      <?php elseif ( get_query_var( 'for' ) === 'teens' ) : ?>
      Teens form
      <?php elseif ( get_query_var( 'for' ) === 'adults' ) : ?>
      Adult form
      <?php endif; ?>

      <!-- Events -->
      <?php get_template_part( 'loop', 'event-card' ); ?>

    </main>
  </div>

  </div>



</div> <!-- end #content -->
<?php get_footer(); ?>
