<?php get_header(); ?>
	
	<header class="hero has-background background-base">
		<h1 class="align-center title"><?php single_cat_title(); ?></h1>
		<div class="eightcol center-grid"><?php echo term_description(); ?></div>
	</header>		

	<div id="content" class="has-cards">	

		<main id="main" class="col-lg--eightcol col--centered hero">
			
			<?php get_template_part( 'loop', 'event' ); ?>			

	    </main>

	</div> <!-- end #content -->

<?php get_footer(); ?>